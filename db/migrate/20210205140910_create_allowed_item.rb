# frozen_string_literal: true

class CreateAllowedItem < ActiveRecord::Migration[6.1]
  def change
    create_table :allowed_items do |t|
      t.string :allow_type
      t.string :value

      t.timestamps
    end
  end
end
