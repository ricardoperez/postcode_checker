# frozen_string_literal: true

class CheckController < ApplicationController
  def main
    @form = PostcodeForm.new
  end

  def check
    @form = PostcodeForm.new(postcode_params)

    if @form.valid?
      @coverage = PostcodeCheckerService.check(@form.postcode, AllowedItem.all)
    else
      render :main
    end
  end

  private

  def postcode_params
    params.require(:postcode_form).permit(:postcode)
  end
end
