# frozen_string_literal: true

class AllowedItem < ApplicationRecord
  validates :allow_type, inclusion: { in: %w[LSOA postcode] }
  validates :value, presence: true

  def self.add_allowed_lsoa(lsoa_code)
    create!(allow_type: 'LSOA', value: lsoa_code)
  end

  def self.add_allowed_postcode(postcode)
    create!(allow_type: 'postcode', value: postcode)
  end
end
