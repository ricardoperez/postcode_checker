# frozen_string_literal: true

class LSOAStrategy
  def initialize(postcode, allowed_lsoa)
    @postcode = postcode
    @allowed_lsoa = allowed_lsoa
  end

  def check
    allowed_lsoa == lsoa.code
  rescue RestClient::NotFound => _e
    false
  end

  private

  attr_reader :postcode, :allowed_lsoa

  def lsoa
    PostcodesClient.lookup(postcode)
  end
end
