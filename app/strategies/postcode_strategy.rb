# frozen_string_literal: true

class PostcodeStrategy
  include ApplicationHelper

  def initialize(given_postcode, allowed_postcode)
    @given_postcode = clean_postcode(given_postcode)
    @allowed_postcode = clean_postcode(allowed_postcode)
  end

  def check
    allowed_postcode == given_postcode
  end

  private

  attr_reader :allowed_postcode, :given_postcode
end
