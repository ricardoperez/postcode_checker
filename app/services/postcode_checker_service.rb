# frozen_string_literal: true

class PostcodeCheckerService
  attr_reader :postcode, :allowed_list

  def initialize(postcode, allowed_list)
    @postcode = postcode
    @allowed_list = allowed_list
  end

  def self.check(*args)
    new(*args).check
  end

  def check
    allowed_list.each do |allowed_item|
      result = call_strategy(allowed_item[:allow_type], allowed_item[:value])
      return true if result
    end

    false
  end

  private

  def call_strategy(type, allowed_value)
    klass = "#{type}Strategy".camelcase.constantize
    strategy = klass.new(postcode, allowed_value)
    strategy.check
  end
end
