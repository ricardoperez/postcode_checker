# frozen_string_literal: true

module ApplicationHelper
  module_function

  def clean_postcode(postcode)
    postcode.gsub(/\s/, '').downcase
  end
end
