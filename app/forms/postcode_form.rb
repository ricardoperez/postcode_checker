# frozen_string_literal: true

class PostcodeForm
  include ActiveModel::Model

  attr_accessor :postcode

  validates :postcode, presence: true, format: { with: /\A[\w\s]+\z/, message: 'Postcode format is invalid' }
end
