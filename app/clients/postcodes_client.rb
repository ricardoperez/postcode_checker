# frozen_string_literal: true

class PostcodesClient
  BASE_URL = 'http://postcodes.io/postcodes'

  def self.lookup(postcode)
    clean_postcode = postcode.remove(/\s+/)
    response = RestClient.get("#{BASE_URL}/#{clean_postcode}")
    create_lsoa(response.body)
  end

  def self.create_lsoa(response_body)
    parsed_body = JSON.parse(response_body)
    result = parsed_body['result']

    OpenStruct.new(code: result['codes']['lsoa'])
  end
end
