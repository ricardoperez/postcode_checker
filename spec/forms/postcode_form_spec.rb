# frozen_string_literal: true

require 'rails_helper'

RSpec.describe PostcodeForm do
  describe '#valid?' do
    subject(:form) { described_class.new(params) }

    context 'when postcode is valid' do
      let(:params) { { 'postcode' => 'S311 7RT' } }

      it do
        expect(form.valid?).to be(true)
      end
    end

    context 'when postcode is empty' do
      let(:params) { {} }

      it do
        expect(form.valid?).to be(false)
      end
    end

    context 'when postcode is invalid' do
      let(:params) { { 'postcode' => '%^&8213S' } }

      it do
        expect(form.valid?).to be(false)
      end
    end

    context 'when postcode has no space' do
      let(:params) { { 'postcode' => 'SE117RT' } }

      it do
        expect(form.valid?).to be(true)
      end
    end
  end
end
