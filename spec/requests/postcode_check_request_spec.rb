# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'PostcodeCheck', type: :request do
  describe 'GET /' do
    it 'renders a successful response' do
      get root_path

      expect(response).to be_successful
    end

    it 'renders postcode check from' do
      get root_path

      expect(response).to render_template(:main)
    end
  end

  describe 'POST /check' do
    context 'when postcode check is successful' do
      let(:allowed_postcode) { 'SE117RT' }

      before do
        AllowedItem.add_allowed_postcode(allowed_postcode)
      end

      it 'renders page with success message' do
        post '/check', params: { postcode_form: { postcode: allowed_postcode } }

        expect(response.body).to include('Area is covered')
      end
    end

    context 'when postcode check by LSOA is successful' do
      let(:mocked_body) { File.read('spec/support/lookup.json') }
      let(:mocked_response) { instance_double('RestClient::Response', body: mocked_body) }

      before do
        allow(RestClient).to receive(:get).and_return(mocked_response)
        AllowedItem.add_allowed_lsoa('E01033325')
      end

      it 'renders page with success message' do
        post '/check', params: { postcode_form: { postcode: 'SA13 930' } }

        expect(response.body).to include('Area is covered')
      end
    end

    context 'when postcode is not in the covered area' do
      it 'renders page with success message' do
        post '/check', params: { postcode_form: { postcode: 'SB11 234' } }

        expect(response.body).to include('Area is not covered')
      end
    end
  end
end
