# frozen_string_literal: true

require 'rails_helper'

RSpec.describe PostcodesClient do
  describe '.lookup' do
    subject(:lookup) { described_class.lookup(postcode) }

    let(:postcode) { 'SE11 1RT' }

    context 'when the postcode lookup response is successful' do
      let(:mocked_body) { File.read('spec/support/lookup.json') }
      let(:mocked_response) { instance_double('RestClient::Response', body: mocked_body) }

      before do
        allow(RestClient).to receive(:get).and_return(mocked_response)
      end

      it do
        expect(lookup.code).to eq('E01033325')
      end
    end

    context 'when the postcode lookup fails' do
      before do
        allow(RestClient).to receive(:get).and_raise(RestClient::NotFound)
      end

      it do
        expect { lookup }.to raise_error(RestClient::NotFound)
      end
    end
  end
end
