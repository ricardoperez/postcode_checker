# frozen_string_literal: true

require 'rails_helper'

RSpec.describe AllowedItem, type: :model do
  describe '#valid?' do
    context 'when params are correct' do
      subject(:item) { described_class.new(value: 'SE137RT', allow_type: 'postcode') }

      it do
        expect(item.valid?).to be(true)
      end
    end

    context 'when allowed_item is not given', type: :model do
      subject(:item) { described_class.new(value: 'SE137RT') }

      it do
        expect(item.valid?).to be(false)
      end
    end

    context 'when allowed_item is invalid', type: :model do
      subject(:item) { described_class.new(value: 'SE137RT', allow_type: 'Smr') }

      it do
        expect(item.valid?).to be(false)
      end
    end

    context 'when value is not give', type: :model do
      subject(:item) { described_class.new(allow_type: 'Smr') }

      it do
        expect(item.valid?).to be(false)
      end
    end
  end

  describe '#add_allowed_lsoa' do
    context 'when created with success' do
      subject(:created_lsoa) { described_class.add_allowed_lsoa('LSAA44H') }

      it 'is persited' do
        expect(created_lsoa.persisted?).to eq(true)
      end

      it do
        expect(created_lsoa.allow_type).to eq('LSOA')
      end
    end
  end

  describe '#add_allowed_postcode' do
    subject(:created_postcode) { described_class.add_allowed_postcode('SE11 7TT') }

    it 'is persited' do
      expect(created_postcode.persisted?).to eq(true)
    end

    it do
      expect(created_postcode.allow_type).to eq('postcode')
    end
  end
end
