# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Postcode check', type: :system do
  before do
    AllowedItem.add_allowed_postcode('SE117RT')
  end

  it 'shows the right content' do
    visit root_path
    expect(page).to have_content('UK valid postcode')
  end

  context 'when postcode area is covered' do
    it do
      visit root_path
      fill_in('postcode', with: 'SE117RT')
      click_button('Check')

      expect(page).to have_content('Area is covered')
    end
  end

  context 'when postcode area has space' do
    it do
      visit root_path
      fill_in('postcode', with: 'SE11 7RT')
      click_button('Check')

      expect(page).to have_content('Area is covered')
    end
  end

  context 'when postcode area is case insensitive' do
    it do
      visit root_path
      fill_in('postcode', with: 'sE117rt')
      click_button('Check')

      expect(page).to have_content('Area is covered')
    end
  end

  context 'when postcode area is not covered' do
    it do
      visit root_path
      fill_in('postcode', with: 'SE127RT')
      click_button('Check')

      expect(page).to have_content('Area is not covered')
    end
  end

  context 'when postcode is invalid' do
    it do
      visit root_path
      fill_in('postcode', with: '%^&8213')
      click_button('Check')

      expect(page).to have_content('Postcode format is invalid')
    end
  end

  context 'when postcode aread is covered through LSOA' do
    before do
      AllowedItem.add_allowed_lsoa('E01003930')
    end

    it do
      visit root_path
      fill_in('postcode', with: 'SE17QD')
      click_button('Check')

      expect(page).to have_content('Area is covered')
    end
  end
end
