# frozen_string_literal: true

require 'rails_helper'

RSpec.describe PostcodeStrategy do
  subject(:strategy) { described_class.new(given_postcode, allowed_postcode) }

  let(:allowed_postcode) { 'SH241AA' }

  describe '#check' do
    context 'when a postcode is allowed' do
      let(:given_postcode) { 'SH241AA' }

      it do
        expect(strategy.check).to be true
      end
    end

    context 'when a postcode is downcased' do
      let(:given_postcode) { 'sh241aa' }

      it do
        expect(strategy.check).to be true
      end
    end

    context 'when a postcode has space' do
      let(:given_postcode) { 'sh24 1aa' }

      it do
        expect(strategy.check).to be true
      end
    end

    context 'when a postcode is not allowed' do
      let(:given_postcode) { 'SH241A' }

      it do
        expect(strategy.check).to be false
      end
    end
  end
end
