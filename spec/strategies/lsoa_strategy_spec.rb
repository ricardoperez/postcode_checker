# frozen_string_literal: true

require 'rails_helper'

RSpec.describe LSOAStrategy do
  subject(:strategy) { described_class.new(given_postcode, allowed_lsoa) }

  let(:given_postcode) { 'SH241AA' }
  let(:allowed_lsoa) { 'E01003981' }

  describe '#check' do
    context 'when a postcode is allowed' do
      let(:lsoa_obj) { OpenStruct.new(code: 'E01003981') }

      before do
        allow(PostcodesClient).to receive(:lookup).and_return(lsoa_obj)
      end

      it do
        expect(strategy.check).to be true
      end
    end

    context 'when a postcode is not allowed' do
      let(:lsoa_obj) { OpenStruct.new(code: 'E01033325') }

      before do
        allow(PostcodesClient).to receive(:lookup).and_return(lsoa_obj)
      end

      it do
        expect(strategy.check).to be false
      end
    end

    context 'when a postcode is invalid' do
      before do
        allow(PostcodesClient).to receive(:lookup).and_raise(RestClient::NotFound)
      end

      it do
        expect(strategy.check).to be false
      end
    end
  end
end
