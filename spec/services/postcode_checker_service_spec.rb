# frozen_string_literal: true

require 'rails_helper'

RSpec.describe PostcodeCheckerService do
  subject(:service) { described_class.new(postcode, allowed_list) }

  describe '#check' do
    context 'when checking allowed postcode' do
      let(:allowed_list) do
        [
          { allow_type: 'postcode',  value: 'SH24 1AB' },
          { allow_type: 'postcode',  value: 'SH24 1AA' }
        ]
      end

      context 'when a postcode is in postcode allow list' do
        let(:postcode) { 'SH24 1AA' }

        it do
          expect(service.check).to be(true)
        end
      end

      context 'when the given postcode is not in the allowed list' do
        let(:postcode) { 'SE11 1RT' }

        it do
          expect(service.check).to be(false)
        end
      end
    end

    context 'when checking allowed LSOA' do
      let(:postcode) { 'SE11 1RT' }

      let(:allowed_list) do
        [
          { allow_type: 'postcode', value: 'SH24 1AB' },
          { allow_type: 'postcode', value: 'SH24 1AA' },
          { allow_type: 'LSOA', value: 'E01033325' }
        ]
      end

      context 'when a postcode is in LSOA allow list' do
        let(:strategy_double) do
          instance_double('LSOAStrategy', check: true)
        end

        before do
          allow(LSOAStrategy).to receive(:new).and_return(strategy_double)
        end

        it do
          expect(service.check).to be(true)
        end
      end

      context 'when a postcode is not in LSOA allow list' do
        let(:strategy_double) do
          instance_double('LSOAStrategy', check: false)
        end

        before do
          allow(LSOAStrategy).to receive(:new).and_return(strategy_double)
        end

        it do
          expect(service.check).to be(false)
        end
      end
    end
  end
end
