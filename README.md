# Postcode Checker

Small project which checks if a postcode is in a Covered area, checking for LSOA and postcode allow list.

### LSOA

Allowed LSOA used the `lsoa_code` fetched from postcodes.io API.

For instance instead of `Lambeth` the database will check for `E01032582`.

So it is needed to check http://postcodes.io/postcodes/SE1%207XA result to get the `lsoa_code`

### Postcodes API

Check http://postcodes.io/ for API documentation and usage.

## Requirements and Installation

#### Ruby version

- 2.7.1

#### Rails version

- 6.1

#### System dependencies

- Google Chrome

### Installation, db creation and seeding

    bin/setup

#### Run the server

    bin/rails server

#### How to run the test suite

    bundle exec rspec

#### How to run rubocop and reek

    bundle exec rubocop
    bundle exec reek

### Adding a item to allowed list

    bin/rails console

```ruby
# Adding a postcode to allowed list
AllowedItem.add_allowed_postcode('SE117RT')

# Adding a LSOA area to allowed list
AllowedItem.add_allowed_postcode('E01032582')

```
